# Freelancer

Web system for KV6002 Team Project and Professionalism.

# Group members

*  Charlie Biddiscombe
*  Alex Gibson
*  Ethan Roe
*  Matthew Randle
*  Jaskeiran Singh

# Installation

Instructions to install the system on a local machine can be found in the 'installation instructions.txt' file.

# GitLab repository

[Click to go to our GitLab repo](https://gitlab.com/MatthewRandle/freelancer/)

